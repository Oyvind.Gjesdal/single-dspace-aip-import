<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:ds="http://data.ub.uib.no/f/dspace"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:param name="aip-path" select="'/home/oyvind/repos/digitalt/gamle-boker'"/>
    <xsl:param name="dspace-user" select="''"/>
    <xsl:param name="packager-opts" select="'-w -r -u -f -t AIP'"/>
    <xsl:param name="java-opts" select="'-Xmx1042m -Dfile.encoding=UTF-8'"/>
    <xsl:param name="dspace-build" select="'/dspace/digitalt/build'"/>
    <xsl:param name="aip-user" select="'tomcat'"/>
    <xsl:param name="debug" select="true()"></xsl:param>
    
    <xsl:output method="text"/>
    
    <xsl:template match="/">
        <xsl:if test="$debug">
    <xsl:message select="$aip-path"/>
        </xsl:if>
        <xsl:variable name="aip-key-regex" select="'^.+@(.+)\.zip'"/>
        <xsl:variable name="sort-by-local-handle-regex" select="'^.+-([^-]+)'"/>
        <xsl:variable name="collections" as="map(*)?">
            <xsl:map>
                <xsl:for-each select="collection($aip-path || '?select=COLLECTION*;metadata=yes')">
                    <!-- get local name of handle as key to sort by -->
                    <xsl:map-entry key="replace(.('name'),$aip-key-regex,'$1')" select="."/>
                </xsl:for-each>
            </xsl:map>
        </xsl:variable>
        
        <xsl:variable name="communities" as="map(*)?">
            <xsl:map>
                <xsl:for-each select="collection($aip-path || '?select=COMMUNITY*;metadata=yes')">
                    <xsl:map-entry key="replace(.('name'),$aip-key-regex,'$1')" select="."/>
                </xsl:for-each>
            </xsl:map>
        </xsl:variable>
        
        <xsl:variable name="items" as="map(*)?">
            <xsl:map>
                <xsl:for-each select="collection($aip-path || '?select=ITEM*;metadata=yes')">
                    <xsl:map-entry key="replace(.('name'),$aip-key-regex,'$1')" select="."/>
                </xsl:for-each>
            </xsl:map>
        </xsl:variable>
        <xsl:text>#!/bin/bash
set -e
</xsl:text>
        <xsl:for-each select="map:keys($communities)">
            <xsl:sort select="xs:integer(replace(.,$sort-by-local-handle-regex,'$1'))"/>
            <xsl:sequence select="ds:aip-export-line($communities(.))"/>
        </xsl:for-each>
        
        <xsl:for-each select="map:keys($collections)">
            <xsl:sort select="xs:integer(replace(.,$sort-by-local-handle-regex,'$1'))"/>
            <xsl:sequence select="ds:aip-export-line($collections(.))"/>
        </xsl:for-each>
        
        <xsl:for-each select="map:keys($items)">
            <xsl:sort select="xs:integer(replace(.,$sort-by-local-handle-regex,'$1'))"/>
            <xsl:sequence select="ds:aip-export-line($items(.))"/>            
        </xsl:for-each>    
  </xsl:template>
    
    <xsl:function name="ds:aip-export-line">
        <xsl:param name="single-package" as="map(*)"/>
        <xsl:text expand-text="1">sudo -u tomcat JAVA_OPTS="{$java-opts}" {$dspace-build}/bin/dspace packager {$packager-opts} -e {$dspace-user} {replace($single-package('name'),'^file:','')}
echo '{tokenize($single-package('name'),'/')[last()]} processed'
</xsl:text>        
    </xsl:function>
</xsl:stylesheet>