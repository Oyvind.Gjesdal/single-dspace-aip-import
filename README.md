# Single DSpace AIP import

Takes an AIP folder as input, and writes a script to produce single aip packages

* Items are sorted by Community, Collection then Item
* All handles are then sorted by number

When importing, first add the original exported zip-file, with a *non-standard* name. Or the original exported-zip file could use the same naming scheme as the recursive AIP-export, before generating the scripts.

Requires ant and java installed.

```
git clone https://git.app.uib.no/Oyvind.Gjesdal/single-dspace-aip-import.git
cd single-dspace-aip-import
ant -Daip.script.name=test.sh -Daip.path=/dspace/digitalt/temp/aip/krigstrykk -Daip.dspace.user=example@uib.no -Daip.dspace.build=/dspace/digitalt/build
```
test.sh is then found in the folder where ant was run from. All AIPs in krigstrykk are added to the script, the eperson example@uib.no is used, and the script looks for the dspace binary in 
/dspace/digitalt/build/bin/dspace.

is the minimal required options.

The script can then be called by 
```
sudo sh test.sh
```

There are some additional options with default values set

```
# setting with default values
-Daip.user=tomcat
-Daip.java_opts="-Xmx1042m -Dfile.encoding=UTF-8"
-Daip.packager_opts="'-w -r -u -f -t AIP"
```

Example output of test.sh:
```
#!/bin/bash
set -e
sudo -u tomcat JAVA_OPTS="-Xmx1042m -Dfile.encoding=UTF-8" /dspace/digitalt/build/bin/dspace packager -w -r -u -f -t AIP -e  /home/oyvind/repos/digitalt/gamle-boker/COLLECTION@1956.2-2661.zip
echo 'COLLECTION@1956.2-2661.zip processed'
sudo -u tomcat JAVA_OPTS="-Xmx1042m -Dfile.encoding=UTF-8" /dspace/digitalt/build/bin/dspace packager -w -r -u -f -t AIP -e  /home/oyvind/repos/digitalt/gamle-boker/COLLECTION@1956.2-2668.zip
echo 'COLLECTION@1956.2-2668.zip processed'
sudo -u tomcat JAVA_OPTS="-Xmx1042m -Dfile.encoding=UTF-8" /dspace/digitalt/build/bin/dspace packager -w -r -u -f -t AIP -e  /home/oyvind/repos/digitalt/gamle-boker/COLLECTION@1956.2-2737.zip
echo 'COLLECTION@1956.2-2737.zip processed'
sudo -u tomcat JAVA_OPTS="-Xmx1042m -Dfile.encoding=UTF-8" /dspace/digitalt/build/bin/dspace packager -w -r -u -f -t AIP -e  /home/oyvind/repos/digitalt/gamle-boker/COLLECTION@1956.2-2882.zip
echo 'COLLECTION@1956.2-2882.zip processed'
sudo -u tomcat JAVA_OPTS="-Xmx1042m -Dfile.encoding=UTF-8" /dspace/digitalt/build/bin/dspace packager -w -r -u -f -t AIP -e  /home/oyvind/repos/digitalt/gamle-boker/COLLECTION@1956.2-2886.zip
echo 'COLLECTION@1956.2-2886.zip processed'
....
```
